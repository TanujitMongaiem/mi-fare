using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MiFare_Programming
{
    public partial class MiFareCardProg : Form
    {

        public int retCode, hContext, hCard, Protocol;
        public bool connActive = false;
        public bool autoDet;
        public byte[] SendBuff = new byte[263];
        public byte[] RecvBuff = new byte[263];
        public int SendLen, RecvLen, nBytesRet, reqType, Aprotocol, dwProtocol, cbPciLength;
        public ModWinsCard.SCARD_READERSTATE RdrState;
        public ModWinsCard.SCARD_IO_REQUEST pioSendRequest;
        public MiFareCardProg()
        {
            InitializeComponent();
        }


        private void InitMenu()
        {
            connActive = false;
            cbReader.Text = "";
            bInit.Enabled = true;
            bConnect.Enabled = false;         
           
            gbAuth.Enabled = false;
            gbStudentID.Enabled = true;
            gbWrite.Enabled = false;
            lStudentID.Text = "";
            lWriteStudentID.Text = "";
            lConnectionStatus.Text = "";
        }

        private void EnableButtons()
        {

            bInit.Enabled = false;
            bConnect.Enabled = true;            

        }

        private void ClearBuffers()
        {

            long indx;

            for (indx = 0; indx <= 262; indx++)
            {

                RecvBuff[indx] = 0;
                SendBuff[indx] = 0;

            }

        }
  
        private void bBarcodeReader_Click(object sender, EventArgs e)
        {
            
            Form2 frm = new Form2();
            frm.ShowDialog();
            this.lStudentID.Text = frm.sentStudentID;
            if(lStudentID.Text != "")
            {
                gbWrite.Enabled = true;
                if(lConnectionStatus.Text == "Successful connection to ACS ACR122 0")
                {
                    bWriteStudentID.Enabled = true;
                }
                else
                {
                    bWriteStudentID.Enabled = false;
                }
                lWriteStudentID.Text = lStudentID.Text;
            }
        }

        private void bInit_Click(object sender, EventArgs e)
        {

            string ReaderList = "" + Convert.ToChar(0);
            int indx;
            int pcchReaders = 0;
            string rName = "";

            //Establish Context
            retCode = ModWinsCard.SCardEstablishContext(ModWinsCard.SCARD_SCOPE_USER, 0, 0, ref hContext);

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {

                //displayOut(1, retCode, "");

                return;

            }

            // 2. List PC/SC card readers installed in the system

            retCode = ModWinsCard.SCardListReaders(this.hContext, null, null, ref pcchReaders);

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {

                //displayOut(1, retCode, "");

                return;
            }

            EnableButtons();

            byte[] ReadersList = new byte[pcchReaders];

            // Fill reader list
            retCode = ModWinsCard.SCardListReaders(this.hContext, null, ReadersList, ref pcchReaders);

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {
               //mMsg.AppendText("SCardListReaders Error: " + ModWinsCard.GetScardErrMsg(retCode));

                return;
            }
            else
            {
                //displayOut(0, 0, " ");
            }

            rName = "";
            indx = 0;

            //Convert reader buffer to string
            while (ReadersList[indx] != 0)
            {

                while (ReadersList[indx] != 0)
                {
                    rName = rName + (char)ReadersList[indx];
                    indx = indx + 1;
                }

                //Add reader name to list
                cbReader.Items.Add(rName);
                rName = "";
                indx = indx + 1;

            }

            if (cbReader.Items.Count > 0)
            {
                cbReader.SelectedIndex = 0;

            }
           
        }

        private void bConnect_Click(object sender, EventArgs e)
        {

            retCode = ModWinsCard.SCardConnect(hContext, cbReader.SelectedItem.ToString(), ModWinsCard.SCARD_SHARE_SHARED,
                                              ModWinsCard.SCARD_PROTOCOL_T0 | ModWinsCard.SCARD_PROTOCOL_T1, ref hCard, ref Protocol);

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {
                lConnectionStatus.ForeColor = Color.Red;
                lConnectionStatus.Text = "Connection Failed";
                //displayOut(1, retCode, "");
            }
            else
            {
                lConnectionStatus.ForeColor = Color.Green;
                lConnectionStatus.Text = "Successful connection to ACS ACR122 0";
                bWriteStudentID.Enabled = true;
                //displayOut(0, 0, "Successful connection to " + cbReader.Text);

            }
            connActive = true;
            gbAuth.Enabled = true;
            gbStudentID.Enabled = true;
            rbKType1.Checked = true;

            byte tmpLong;
            string tmpStr;

            //if (tKey1.Text == "" | !byte.TryParse(tKey1.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey1.Focus();
            //    tKey1.Text = "";
            //    return;

            //}

            //if (tKey2.Text == "" | !byte.TryParse(tKey2.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey2.Focus();
            //    tKey2.Text = "";
            //    return;
            //}

            //if (tKey3.Text == "" | !byte.TryParse(tKey3.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey3.Focus();
            //    tKey3.Text = "";
            //    return;

            //}

            //if (tKey4.Text == "" | !byte.TryParse(tKey4.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey4.Focus();
            //    tKey4.Text = "";
            //    return;
            //}

            //if (tKey5.Text == "" | !byte.TryParse(tKey5.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey5.Focus();
            //    tKey5.Text = "";
            //    return;

            //}

            //if (tKey6.Text == "" | !byte.TryParse(tKey6.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tKey6.Focus();
            //    tKey6.Text = "";
            //    return;
            //}


            ClearBuffers();
            // Load Authentication Keys command
            SendBuff[0] = 0xFF;                                                                        // Class
            SendBuff[1] = 0x82;                                                                        // INS
            SendBuff[2] = 0x00;                                                                        // P1 : Key Structure
            SendBuff[3] = 0x00;
            SendBuff[4] = 0x06;                                                                        // P3 : Lc
            SendBuff[5] = 0x4B;        // Key 1 value
            SendBuff[6] = 0x4D;        // Key 2 value
            SendBuff[7] = 0x49;        // Key 3 value
            SendBuff[8] = 0x54;        // Key 4 value
            SendBuff[9] = 0x4C;        // Key 5 value
            SendBuff[10] = 0x39;       // Key 6 value

            SendLen = 11;
            RecvLen = 2;

            retCode = SendAPDU();

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {
                return;
            }
            else
            {
                //tTestAuthen.Text = "load key �����";
                tmpStr = "";
                for (int indx = RecvLen - 2; indx <= RecvLen - 1; indx++)
                {

                    tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);

                }

            }
            if (tmpStr.Trim() != "90 00")
            {
                //tTestAuthen.Text = "load key �������";
                //displayOut(4, 0, "Load authentication keys error!");
            }

        }

        public int SendAPDU()
        {
            int indx;
            string tmpStr;

            pioSendRequest.dwProtocol = Aprotocol;
            pioSendRequest.cbPciLength = 8;

            // Display Apdu In
            tmpStr = "";
            for (indx = 0; indx <= SendLen - 1; indx++)
            {

                tmpStr = tmpStr + " " + string.Format("{0:X2}", SendBuff[indx]);

            }
            //displayOut(2, 0, tmpStr);
            retCode = ModWinsCard.SCardTransmit(hCard, ref pioSendRequest, ref SendBuff[0], SendLen, ref pioSendRequest, ref RecvBuff[0], ref RecvLen);

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {

                //displayOut(1, retCode, "");
                return retCode;

            }

            tmpStr = "";
            for (indx = 0; indx <= RecvLen - 1; indx++)
            {

                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);

            }

            //displayOut(3, 0, tmpStr);
            return retCode;

        }

        private void bClear_Click(object sender, EventArgs e)
        {
            //mMsg.Clear();
        }

        private void MiFareCardProg_Load(object sender, EventArgs e)
        {
            InitMenu();
        }

        private void bReset_Click(object sender, EventArgs e)
        {

            if (connActive)
            {

                retCode = ModWinsCard.SCardDisconnect(hCard, ModWinsCard.SCARD_UNPOWER_CARD);

            }

            retCode = ModWinsCard.SCardReleaseContext(hCard);
            InitMenu();
        
        }

        private void bQuit_Click(object sender, EventArgs e)
        {

            // terminate the application
            retCode = ModWinsCard.SCardReleaseContext(hContext);
            retCode = ModWinsCard.SCardDisconnect(hCard, ModWinsCard.SCARD_UNPOWER_CARD);
            System.Environment.Exit(0);

        }

        private void bAuth_Click(object sender, EventArgs e)
        {
            int tempInt, indx;
            byte tmpLong;
            string tmpStr;

            // Validate input
            //if (tBlkNo.Text == "" | !int.TryParse(tBlkNo.Text, out tempInt))
            //{

            //    tBlkNo.Focus();
            //    tBlkNo.Text = "";
            //    return;

            //}

            //if (int.Parse(tBlkNo.Text) > 319) //�ŧ��Ҩҡ string �� int
            //{

            //    tBlkNo.Text = "319";

            //}

            //if (tAuthenKeyNum.Text == "" | !byte.TryParse(tAuthenKeyNum.Text, System.Globalization.NumberStyles.HexNumber, null, out tmpLong))
            //{

            //    tAuthenKeyNum.Focus();
            //    tAuthenKeyNum.Text = "";
            //    return;

            //}
            //else if (int.Parse(tAuthenKeyNum.Text) > 1)
            //{
            //    tAuthenKeyNum.Text = "1";
            //    return;
            //}

            ClearBuffers();

            SendBuff[0] = 0xFF;                             // Class
            SendBuff[1] = 0x86;                             // INS
            SendBuff[2] = 0x00;                             // P1
            SendBuff[3] = 0x00;                             // P2
            SendBuff[4] = 0x05;                             // Lc
            SendBuff[5] = 0x01;                             // Byte 1 : Version number
            SendBuff[6] = 0x00;                             // Byte 2
            SendBuff[7] = 0x37;     // Byte 3 : Block number

            rbKType1.Checked = true;
            SendBuff[8] = 0x60;


            SendBuff[9] = 0x00;        // Key 5 value


            SendLen = 10;
            RecvLen = 2;

            retCode = SendAPDU();

            if (retCode != ModWinsCard.SCARD_S_SUCCESS)
            {
                tTestAuthen.Text = "Auth �������";
                return;
            }
            else
            {
                tTestAuthen.Text = "Auth �����";
                tmpStr = "";
                for (indx = 0; indx <= RecvLen - 1; indx++)
                {

                    tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);

                }

            }
            if (tmpStr.Trim() == "90 00")
            {
                tTestAuthen.Text = "Auth ����� 90 00";
                //displayOut(0, 0, "Authentication success!");
            }
            else
            {
                tTestAuthen.Text = "Auth �������";
                //displayOut(4, 0, "Authentication failed!");
            }


        
        }
    
    }

}