namespace MiFare_Programming
{
    partial class MiFareCardProg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MiFareCardProg));
            this.bReset = new System.Windows.Forms.Button();
            this.bQuit = new System.Windows.Forms.Button();
            this.bConnect = new System.Windows.Forms.Button();
            this.bInit = new System.Windows.Forms.Button();
            this.cbReader = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.bAuth = new System.Windows.Forms.Button();
            this.rbKType2 = new System.Windows.Forms.RadioButton();
            this.gbAuth = new System.Windows.Forms.GroupBox();
            this.tAuthenKeyNum = new System.Windows.Forms.TextBox();
            this.tBlkNo = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.gbKType = new System.Windows.Forms.GroupBox();
            this.rbKType1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lStudentID = new System.Windows.Forms.Label();
            this.bBarcodeReader = new System.Windows.Forms.Button();
            this.gbStudentID = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lConnectionStatus = new System.Windows.Forms.Label();
            this.gbWrite = new System.Windows.Forms.GroupBox();
            this.bWriteStudentID = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lWriteStudentID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tTestAuthen = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gbAuth.SuspendLayout();
            this.gbKType.SuspendLayout();
            this.gbStudentID.SuspendLayout();
            this.gbWrite.SuspendLayout();
            this.SuspendLayout();
            // 
            // bReset
            // 
            this.bReset.Location = new System.Drawing.Point(577, 495);
            this.bReset.Margin = new System.Windows.Forms.Padding(4);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(151, 28);
            this.bReset.TabIndex = 48;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // bQuit
            // 
            this.bQuit.Location = new System.Drawing.Point(744, 495);
            this.bQuit.Margin = new System.Windows.Forms.Padding(4);
            this.bQuit.Name = "bQuit";
            this.bQuit.Size = new System.Drawing.Size(135, 28);
            this.bQuit.TabIndex = 49;
            this.bQuit.Text = "Quit";
            this.bQuit.UseVisualStyleBackColor = true;
            this.bQuit.Click += new System.EventHandler(this.bQuit_Click);
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(723, 106);
            this.bConnect.Margin = new System.Windows.Forms.Padding(4);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(156, 28);
            this.bConnect.TabIndex = 42;
            this.bConnect.Text = "Connect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bInit
            // 
            this.bInit.Location = new System.Drawing.Point(723, 71);
            this.bInit.Margin = new System.Windows.Forms.Padding(4);
            this.bInit.Name = "bInit";
            this.bInit.Size = new System.Drawing.Size(156, 28);
            this.bInit.TabIndex = 41;
            this.bInit.Text = "Initialize";
            this.bInit.UseVisualStyleBackColor = true;
            this.bInit.Click += new System.EventHandler(this.bInit_Click);
            // 
            // cbReader
            // 
            this.cbReader.FormattingEnabled = true;
            this.cbReader.Location = new System.Drawing.Point(592, 37);
            this.cbReader.Margin = new System.Windows.Forms.Padding(4);
            this.cbReader.Name = "cbReader";
            this.cbReader.Size = new System.Drawing.Size(287, 24);
            this.cbReader.TabIndex = 40;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(484, 41);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(98, 17);
            this.Label1.TabIndex = 39;
            this.Label1.Text = "Select Reader";
            // 
            // bAuth
            // 
            this.bAuth.Location = new System.Drawing.Point(225, 137);
            this.bAuth.Margin = new System.Windows.Forms.Padding(4);
            this.bAuth.Name = "bAuth";
            this.bAuth.Size = new System.Drawing.Size(157, 28);
            this.bAuth.TabIndex = 13;
            this.bAuth.Text = "Authenticate";
            this.bAuth.UseVisualStyleBackColor = true;
            this.bAuth.Click += new System.EventHandler(this.bAuth_Click);
            // 
            // rbKType2
            // 
            this.rbKType2.AutoSize = true;
            this.rbKType2.Location = new System.Drawing.Point(21, 65);
            this.rbKType2.Margin = new System.Windows.Forms.Padding(4);
            this.rbKType2.Name = "rbKType2";
            this.rbKType2.Size = new System.Drawing.Size(66, 21);
            this.rbKType2.TabIndex = 2;
            this.rbKType2.TabStop = true;
            this.rbKType2.Text = "Key B";
            this.rbKType2.UseVisualStyleBackColor = true;
            // 
            // gbAuth
            // 
            this.gbAuth.Controls.Add(this.bAuth);
            this.gbAuth.Controls.Add(this.tAuthenKeyNum);
            this.gbAuth.Controls.Add(this.tBlkNo);
            this.gbAuth.Controls.Add(this.Label5);
            this.gbAuth.Controls.Add(this.Label4);
            this.gbAuth.Controls.Add(this.gbKType);
            this.gbAuth.Location = new System.Drawing.Point(487, 298);
            this.gbAuth.Margin = new System.Windows.Forms.Padding(4);
            this.gbAuth.Name = "gbAuth";
            this.gbAuth.Padding = new System.Windows.Forms.Padding(4);
            this.gbAuth.Size = new System.Drawing.Size(396, 180);
            this.gbAuth.TabIndex = 44;
            this.gbAuth.TabStop = false;
            this.gbAuth.Text = "Authentication Function";
            // 
            // tAuthenKeyNum
            // 
            this.tAuthenKeyNum.Location = new System.Drawing.Point(145, 64);
            this.tAuthenKeyNum.Margin = new System.Windows.Forms.Padding(4);
            this.tAuthenKeyNum.MaxLength = 2;
            this.tAuthenKeyNum.Name = "tAuthenKeyNum";
            this.tAuthenKeyNum.Size = new System.Drawing.Size(32, 22);
            this.tAuthenKeyNum.TabIndex = 6;
            // 
            // tBlkNo
            // 
            this.tBlkNo.Location = new System.Drawing.Point(145, 31);
            this.tBlkNo.Margin = new System.Windows.Forms.Padding(4);
            this.tBlkNo.MaxLength = 3;
            this.tBlkNo.Name = "tBlkNo";
            this.tBlkNo.Size = new System.Drawing.Size(32, 22);
            this.tBlkNo.TabIndex = 5;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(9, 64);
            this.Label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(124, 17);
            this.Label5.TabIndex = 3;
            this.Label5.Text = "Key Store Number";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(12, 34);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(103, 17);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "Block No (Dec)";
            // 
            // gbKType
            // 
            this.gbKType.Controls.Add(this.rbKType2);
            this.gbKType.Controls.Add(this.rbKType1);
            this.gbKType.Location = new System.Drawing.Point(225, 11);
            this.gbKType.Margin = new System.Windows.Forms.Padding(4);
            this.gbKType.Name = "gbKType";
            this.gbKType.Padding = new System.Windows.Forms.Padding(4);
            this.gbKType.Size = new System.Drawing.Size(145, 106);
            this.gbKType.TabIndex = 1;
            this.gbKType.TabStop = false;
            this.gbKType.Text = "Key Type";
            // 
            // rbKType1
            // 
            this.rbKType1.AutoSize = true;
            this.rbKType1.Location = new System.Drawing.Point(21, 23);
            this.rbKType1.Margin = new System.Windows.Forms.Padding(4);
            this.rbKType1.Name = "rbKType1";
            this.rbKType1.Size = new System.Drawing.Size(66, 21);
            this.rbKType1.TabIndex = 1;
            this.rbKType1.TabStop = true;
            this.rbKType1.Text = "Key A";
            this.rbKType1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(21, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(341, 29);
            this.label6.TabIndex = 50;
            this.label6.Text = "�������ҹ��¹���ʹѡ�֡��";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(55, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 25);
            this.label7.TabIndex = 51;
            this.label7.Text = "���ʹѡ�֡��";
            // 
            // lStudentID
            // 
            this.lStudentID.AutoSize = true;
            this.lStudentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lStudentID.Location = new System.Drawing.Point(182, 48);
            this.lStudentID.Name = "lStudentID";
            this.lStudentID.Size = new System.Drawing.Size(0, 29);
            this.lStudentID.TabIndex = 52;
            // 
            // bBarcodeReader
            // 
            this.bBarcodeReader.Location = new System.Drawing.Point(170, 104);
            this.bBarcodeReader.Name = "bBarcodeReader";
            this.bBarcodeReader.Size = new System.Drawing.Size(125, 31);
            this.bBarcodeReader.TabIndex = 53;
            this.bBarcodeReader.Text = "��ҹ��Ҩҡ������";
            this.bBarcodeReader.UseVisualStyleBackColor = true;
            this.bBarcodeReader.Click += new System.EventHandler(this.bBarcodeReader_Click);
            // 
            // gbStudentID
            // 
            this.gbStudentID.Controls.Add(this.label7);
            this.gbStudentID.Controls.Add(this.bBarcodeReader);
            this.gbStudentID.Controls.Add(this.lStudentID);
            this.gbStudentID.Location = new System.Drawing.Point(12, 47);
            this.gbStudentID.Name = "gbStudentID";
            this.gbStudentID.Size = new System.Drawing.Size(454, 217);
            this.gbStudentID.TabIndex = 12;
            this.gbStudentID.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(484, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Connection Status :";
            // 
            // lConnectionStatus
            // 
            this.lConnectionStatus.AutoSize = true;
            this.lConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lConnectionStatus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lConnectionStatus.Location = new System.Drawing.Point(614, 9);
            this.lConnectionStatus.Name = "lConnectionStatus";
            this.lConnectionStatus.Size = new System.Drawing.Size(0, 17);
            this.lConnectionStatus.TabIndex = 52;
            // 
            // gbWrite
            // 
            this.gbWrite.Controls.Add(this.bWriteStudentID);
            this.gbWrite.Controls.Add(this.label12);
            this.gbWrite.Controls.Add(this.label13);
            this.gbWrite.Controls.Add(this.label11);
            this.gbWrite.Controls.Add(this.label10);
            this.gbWrite.Controls.Add(this.lWriteStudentID);
            this.gbWrite.Controls.Add(this.label9);
            this.gbWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.gbWrite.ForeColor = System.Drawing.Color.Red;
            this.gbWrite.Location = new System.Drawing.Point(12, 271);
            this.gbWrite.Name = "gbWrite";
            this.gbWrite.Size = new System.Drawing.Size(454, 217);
            this.gbWrite.TabIndex = 54;
            this.gbWrite.TabStop = false;
            this.gbWrite.Text = "��¹���ʹѡ�֡��ŧ�ѵ�";
            // 
            // bWriteStudentID
            // 
            this.bWriteStudentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bWriteStudentID.ForeColor = System.Drawing.Color.Black;
            this.bWriteStudentID.Location = new System.Drawing.Point(288, 164);
            this.bWriteStudentID.Name = "bWriteStudentID";
            this.bWriteStudentID.Size = new System.Drawing.Size(125, 31);
            this.bWriteStudentID.TabIndex = 54;
            this.bWriteStudentID.Text = "WriteTest";
            this.bWriteStudentID.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(270, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 25);
            this.label12.TabIndex = 58;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(25, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(210, 25);
            this.label13.TabIndex = 57;
            this.label13.Text = "ʶҹС����¹������ŧ�ѵ�";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(270, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 25);
            this.label11.TabIndex = 56;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(25, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 25);
            this.label10.TabIndex = 55;
            this.label10.Text = "���ҷ����¹�ѵ�";
            // 
            // lWriteStudentID
            // 
            this.lWriteStudentID.AutoSize = true;
            this.lWriteStudentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lWriteStudentID.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lWriteStudentID.Location = new System.Drawing.Point(268, 44);
            this.lWriteStudentID.Name = "lWriteStudentID";
            this.lWriteStudentID.Size = new System.Drawing.Size(0, 25);
            this.lWriteStudentID.TabIndex = 54;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(25, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 25);
            this.label9.TabIndex = 53;
            this.label9.Text = "���ʹѡ�֡��";
            // 
            // tTestAuthen
            // 
            this.tTestAuthen.Location = new System.Drawing.Point(12, 500);
            this.tTestAuthen.Name = "tTestAuthen";
            this.tTestAuthen.Size = new System.Drawing.Size(454, 22);
            this.tTestAuthen.TabIndex = 55;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(163, 514);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 17);
            this.label14.TabIndex = 56;
            this.label14.Text = "label14";
            // 
            // MiFareCardProg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 536);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tTestAuthen);
            this.Controls.Add(this.gbWrite);
            this.Controls.Add(this.gbStudentID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lConnectionStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.bQuit);
            this.Controls.Add(this.bConnect);
            this.Controls.Add(this.bInit);
            this.Controls.Add(this.cbReader);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.gbAuth);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MiFareCardProg";
            this.Text = "MiFare Card Programming";
            this.Load += new System.EventHandler(this.MiFareCardProg_Load);
            this.gbAuth.ResumeLayout(false);
            this.gbAuth.PerformLayout();
            this.gbKType.ResumeLayout(false);
            this.gbKType.PerformLayout();
            this.gbStudentID.ResumeLayout(false);
            this.gbStudentID.PerformLayout();
            this.gbWrite.ResumeLayout(false);
            this.gbWrite.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button bReset;
        internal System.Windows.Forms.Button bQuit;
        internal System.Windows.Forms.Button bConnect;
        internal System.Windows.Forms.Button bInit;
        internal System.Windows.Forms.ComboBox cbReader;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button bAuth;
        internal System.Windows.Forms.RadioButton rbKType2;
        internal System.Windows.Forms.GroupBox gbAuth;
        internal System.Windows.Forms.TextBox tAuthenKeyNum;
        internal System.Windows.Forms.TextBox tBlkNo;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox gbKType;
        internal System.Windows.Forms.RadioButton rbKType1;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Button bBarcodeReader;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label lConnectionStatus;
        internal System.Windows.Forms.GroupBox gbWrite;
        internal System.Windows.Forms.GroupBox gbStudentID;
        public System.Windows.Forms.Label lStudentID;
        internal System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lWriteStudentID;
        public System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Button bWriteStudentID;
        private System.Windows.Forms.TextBox tTestAuthen;
        private System.Windows.Forms.Label label14;
    }
}

