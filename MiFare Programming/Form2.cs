﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MiFare_Programming
{
    public partial class Form2 : Form
    {
        public static string SetValueForStudentID = "";
        public Form2()
        {
            InitializeComponent();
        }

        private void InitMenu()
        {
            tStudentID.Text = SetValueForStudentID;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            InitMenu();
        }

        public string sentStudentID
        {
            get { return tStudentID.Text; }
        }


        private void tStudentID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && tStudentID.TextLength != 8)
            {
                MessageBox.Show("กรอกเฉพาะตัวเลข 8 ตัวเท่านั้น");
            }
            else if (e.KeyCode == Keys.Enter && tStudentID.TextLength == 8)
            {
                this.Close();
            }
        }

        private void tStudentID_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            //ch เป็นได้เฉพาะ digit และกดได้เฉพาะปุ่ม BACKSPACE key. , DEL key. , ENTER key.
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 13) 
            {
                e.Handled = true;
                MessageBox.Show("Please Enter Valid Value");
            }
        }
    }
}
